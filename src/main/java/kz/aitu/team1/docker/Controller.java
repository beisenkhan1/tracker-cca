package kz.aitu.team1.docker;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Controller {
    List<String> words = new ArrayList<String>();
    @GetMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        words.add(name);
        return String.format("Hello %s!", name);
    }

    @GetMapping("/get-names")
    public String getNames() {
        List<String> words = this.words;
        int l = words.size();
        for(int i = 0; i < l-1; ++i) {
            for (int j = i + 1; j < l; ++j) {
                if (words.get(i).compareTo(words.get(j)) > 0) {
                    String temp = words.get(i);
                    words.set(i, words.get(j));
                    words.set(j, temp);
                }
            }
        }
        StringBuilder output = new StringBuilder();
        for(String s : words) {
            output.append(s).append(", ");
        }
        return output.toString();
    }
}