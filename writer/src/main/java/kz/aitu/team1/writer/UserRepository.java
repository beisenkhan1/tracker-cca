package kz.aitu.team1.writer;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface UserRepository extends JpaRepository<kz.aitu.team1.writer.User, Long> {

    List<kz.aitu.team1.writer.User> findAll();
}
